#!/usr/bin/env bash
date_for_now_and_for_this_script_only="$(date +'%d-%m-%Y')"
cromite_versioning_for_me_only="$(curl -sL https://api.github.com/repos/uazo/cromite/releases/latest | jq -r ".tag_name")"

echo "Getting latest release ..."
rm -rf lib/ *.apk
wget $(curl -L -s https://api.github.com/repos/uazo/cromite/releases/latest | grep -o -E "https://(.*)arm64_ChromePublic(.*).apk")
unzip arm64_ChromePublic.apk

echo "Cleaning ..."
rm -rf *.apk.*
rm -rf assets/ res* AndroidManifest.xml classes.dex
echo "Done! This is your commit message:"
echo "Update Cromite as $date_for_now_and_for_this_script_only and $cromite_versioning_for_me_only"
